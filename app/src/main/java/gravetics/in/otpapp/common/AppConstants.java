package gravetics.in.otpapp.common;

/**
 * Created by satish kadyan on 7/17/2017.
 * common place to manage static and constant variables for aplication
 */

public class AppConstants {

    public static final String BASE_URL = "https://itunes.apple.com/";
    public static final String SONG_DATA = "song_data";

}
