package gravetics.in.otpapp.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by satish kadyan on 7/17/2017.
 * util class for application for providing common reusable quick function in application
 */

public class Utils {

    // generate readable format time from millisseconds
    public static String millisToReadable(long timeMillis) {

        long hours = TimeUnit.MILLISECONDS.toHours(timeMillis);
        long mins = TimeUnit.MILLISECONDS.toMinutes(timeMillis);
        long secs = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        String readableTime = "";
        if (hours > 0) {
            readableTime = String.format("%dh %d min, %d sec",
                    hours,
                    mins - TimeUnit.HOURS.toMinutes(hours),
                    secs - TimeUnit.MINUTES.toSeconds(mins)
            );
        } else {
            readableTime = String.format("%d min, %d sec",
                    mins - TimeUnit.HOURS.toMinutes(hours),
                    secs - TimeUnit.MINUTES.toSeconds(mins)
            );
        }
        return readableTime;

        //return (new SimpleDateFormat("mm:ss:SSS")).format(new Date(timeMillis));

    }

    // format the date to disred format to display in application
    public static String dateFormat(String dateString) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = df.parse(dateString);
            SimpleDateFormat dt1 = new SimpleDateFormat("dd MMMM yyyy");
            return dt1.format(date);
        } catch (ParseException e) {
            return "";
        }

    }


    // format the name from firstname last name combination in unified way at all places in application
    public static String nameFormat(String firstName, String lastName) {
        String nameFormatted = "";
        if (firstName != null && firstName.length() > 0) {
            nameFormatted = firstName;
        }
        if (lastName != null && lastName.length() > 0) {
            if (nameFormatted.length() > 0) {
                nameFormatted = nameFormatted + " " + lastName;

            } else {
                nameFormatted = lastName;

            }
        }
        return nameFormatted;
    }

}
