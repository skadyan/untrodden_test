package gravetics.in.otpapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import gravetics.in.otpapp.R;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getName();
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //butterknife bind view call to bind views to fields with annotation, get rid of calling and casting findbyview repeatly
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {

        //set material toolbar, instead of old fashioned actionbar
        setSupportActionBar(myToolbar);
//this is comment abc
    }

}
