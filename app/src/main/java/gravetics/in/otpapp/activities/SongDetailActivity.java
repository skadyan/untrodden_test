package gravetics.in.otpapp.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.common.Utils;
import gravetics.in.otpapp.model.Song;

public class SongDetailActivity extends AppCompatActivity {

    private Song song;
    @BindView(R.id.trackName)
    TextView trackName;
    @BindView(R.id.trackArtist)
    TextView trackArtist;
    @BindView(R.id.genre)
    TextView trackGenre;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.duration)
    TextView duration;
    @BindView(R.id.releaseDate)
    TextView releaseDate;
    @BindView(R.id.country)
    TextView country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_detail);

        //butterknife bind view call to bind views to fields with annotation, get rid of calling and casting findviewbyid repeatly
        ButterKnife.bind(this);

        // get song detail data from intent passed from preious activity
        song = getIntent().getParcelableExtra(AppConstants.SONG_DATA);

        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(getString(R.string.activity_songdetail_title));
        Glide.with(this).load(song.getArtworkUrl100()).into(image);
        trackName.setText(song.getTrackName());
        trackArtist.setText(getString(R.string.prefix_artist) + song.getArtistName());
        trackGenre.setText(getString(R.string.prefix_genre) + song.getPrimaryGenreName());
        price.setText(song.getCurrency() + " " + song.getTrackPrice());
        duration.setText(Utils.millisToReadable(song.getTrackTimeMillis()));

        releaseDate.setText(Utils.dateFormat(song.getReleaseDate()));
        country.setText(song.getCountry());

    }

}