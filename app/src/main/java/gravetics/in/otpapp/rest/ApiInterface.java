package gravetics.in.otpapp.rest;

import gravetics.in.otpapp.model.SongsResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by satish kadyan on 7/17/2017.
 * interface used by retrofit to perform rest api in easy way
 */

public interface ApiInterface {

    // return songs list from server
    @GET("search")
    Observable<SongsResponse> doGetSongs(@Query("term") String term);
}
