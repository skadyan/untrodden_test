package gravetics.in.otpapp.model;

import android.content.Context;

import gravetics.in.otpapp.rest.APIClient;
import gravetics.in.otpapp.rest.ApiInterface;
import io.reactivex.Observable;

/**
 * Created by satish kadyan on 7/17/2017.
 * class to manage the Songs api calls
 */

public class SongManager {
    private ApiInterface apiInterface;

    public SongManager(Context context) {
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
    }

    //retreive songs list with retrofit and return as obervable to caller
    public Observable<SongsResponse> doGetSongs(String term) {
        return apiInterface.doGetSongs(term);
    }
}
