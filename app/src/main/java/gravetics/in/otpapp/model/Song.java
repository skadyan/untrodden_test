package gravetics.in.otpapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by satish kadyan on 7/17/2017.
 * POJO Song model to represent song entity
 */

public class Song implements Parcelable {

    @SerializedName("trackName")
    private String trackName;

    @SerializedName("artistName")
    private String artistName;

    @SerializedName("primaryGenreName")
    private String primaryGenreName;

    @SerializedName("trackTimeMillis")
    private long trackTimeMillis;

    @SerializedName("trackPrice")
    private String trackPrice;

    @SerializedName("artworkUrl100")
    private String artworkUrl100;

    @SerializedName("currency")
    private String currency;

    @SerializedName("releaseDate")
    private String releaseDate;

    @SerializedName("country")
    private String country;

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    public void setPrimaryGenreName(String primaryGenreName) {
        this.primaryGenreName = primaryGenreName;
    }

    public long getTrackTimeMillis() {
        return trackTimeMillis;
    }

    public void setTrackTimeMillis(long trackTimeMillis) {
        this.trackTimeMillis = trackTimeMillis;
    }

    public String getTrackPrice() {
        return trackPrice;
    }

    public void setTrackPrice(String trackPrice) {
        this.trackPrice = trackPrice;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public void setArtworkUrl100(String artworkUrl100) {
        this.artworkUrl100 = artworkUrl100;
    }

    // making this class parcelable, to able to pass it as bundle with intent to different activities easily
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(trackName);
        dest.writeString(artistName);
        dest.writeString(primaryGenreName);
        dest.writeLong(trackTimeMillis);
        dest.writeString(trackPrice);
        dest.writeString(artworkUrl100);
        dest.writeString(currency);
        dest.writeString(releaseDate);
        dest.writeString(country);
    }

    private Song(Parcel in) {
        this.trackName = in.readString();
        this.artistName = in.readString();
        this.primaryGenreName = in.readString();
        this.trackTimeMillis = in.readLong();
        this.trackPrice = in.readString();
        this.artworkUrl100 = in.readString();
        this.currency = in.readString();
        this.releaseDate = in.readString();
        this.country = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {

        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    @Override
    public String toString() {
        return "Song{" +
                "trackName='" + trackName + '\'' +
                ", artistName='" + artistName + '\'' +
                ", primaryGenreName='" + primaryGenreName + '\'' +
                ", trackTimeMillis='" + trackTimeMillis + '\'' +
                ", trackPrice='" + trackPrice + '\'' +
                ", artworkUrl100='" + artworkUrl100 + '\'' +
                '}';
    }
}