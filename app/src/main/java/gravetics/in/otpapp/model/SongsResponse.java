package gravetics.in.otpapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by satish kadyan on 7/17/2017.
 * mapping class for automatically mapping of json data returned from server call to Object representation by retrofit convertor factory
 */

public class SongsResponse {
    @SerializedName("results")
    private List<Song> data;

    @SerializedName("resultCount")
    private int resultCount;

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public List<Song> getResults() {
        return data;
    }

    public void setResults(List<Song> data) {
        this.data = data;
    }

}
