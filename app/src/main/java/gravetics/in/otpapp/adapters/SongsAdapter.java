package gravetics.in.otpapp.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import gravetics.in.otpapp.R;
import gravetics.in.otpapp.common.Utils;
import gravetics.in.otpapp.model.Song;

/**
 * Created by satish kadyan on 7/17/2017.
 */

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.ViewHolder> {

    private final Activity activity;
    private ArrayList<Song> songList;

    public SongsAdapter(ArrayList<Song> songList, Activity activity) {
        this.songList = songList;
        this.activity = activity;
    }

    @Override
    public SongsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_song, parent, false);
        return new SongsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SongsAdapter.ViewHolder holder, int position) {
        Song song = songList.get(position);
        holder.rootItem.setTag(song);
        holder.price.setText(song.getCurrency() + " " + song.getTrackPrice());
        holder.trackArtist.setText(activity.getString(R.string.prefix_artist) + song.getArtistName());
        holder.trackName.setText(song.getTrackName());
        holder.genre.setText(activity.getString(R.string.prefix_genre) + song.getPrimaryGenreName());
        holder.duration.setText(Utils.millisToReadable(song.getTrackTimeMillis()));
        Glide.with(activity)
                .load(song.getArtworkUrl100())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    // updating data of adapter and refresh the list to update list display new data
    public void setResults(ArrayList<Song> songs) {
        songList = songs;
        notifyDataSetChanged();
    }

    // reusable viewholder pattern for better reusing views rows in listview
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView price;
        private TextView trackName;
        private TextView trackArtist;
        private TextView duration;
        private TextView genre;
        private ImageView image;
        private LinearLayout rootItem;

        public ViewHolder(View itemView) {
            super(itemView);
            price = (TextView) itemView.findViewById(R.id.price);
            trackName = (TextView) itemView.findViewById(R.id.trackName);
            trackArtist = (TextView) itemView.findViewById(R.id.trackArtist);
            duration = (TextView) itemView.findViewById(R.id.duration);
            genre = (TextView) itemView.findViewById(R.id.genre);
            image = (ImageView) itemView.findViewById(R.id.image);
            rootItem = (LinearLayout) itemView.findViewById(R.id.root_item);
        }
    }
}
