package gravetics.in.otpapp.network;

import java.io.IOException;

/**
 * Created by satish kadyan on 7/17/2017.
 * exception for handling no internet connection status
 */

public class NoNetworkConnectionException extends IOException {

    public NoNetworkConnectionException() {
    }

    @Override
    public String getMessage() {
        return "No internet connection";
    }
}

