package gravetics.in.otpapp.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.activities.SongDetailActivity;
import gravetics.in.otpapp.adapters.SongsAdapter;
import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.listeners.SongClickListener;
import gravetics.in.otpapp.model.Song;
import gravetics.in.otpapp.model.SongManager;
import gravetics.in.otpapp.model.SongsResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by satish kadyan on 7/17/2017.
 * fragment to display list of songs
 */

public class SongsListFragment extends Fragment implements SongClickListener.OnItemClickListener, SearchView.OnQueryTextListener {

    private final static String TAG = SongsListFragment.class.getName();
    private CompositeDisposable mCompositeDisposable;
    private Snackbar errorSnackbar;
    private SongsAdapter adapter;

    @BindView(R.id.error_text)
    TextView emptyView;
    @BindView(R.id.progressView)
    ProgressBar mProgressView;
    @BindView(R.id.card_recycler_view)
    RecyclerView mRecyclerView;
    private ArrayList<Song> retainData = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        SearchManager searchManager = (SearchManager)
                getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getActivity().getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        query = query.trim();
        if (query.length() > 0 && query != "") {
            Toast.makeText(getActivity(), "Searching for " + query, Toast.LENGTH_LONG).show();
            loadSongs(query);
        } else {
            Toast.makeText(getActivity(), "Please enter valid search term", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_songlist, container, false);

        //bind view to variable annotated with butterknife
        ButterKnife.bind(this, view);

        //RXjava disposable to handle async call to get list of songs on other thread, and display back them in listview on main/UI thread
        mCompositeDisposable = new CompositeDisposable();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        ArrayList songs = new ArrayList<>();
        adapter = new SongsAdapter(songs, getActivity());
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnItemTouchListener(new SongClickListener(getActivity().getApplicationContext(), this));
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL
        );
        mRecyclerView.addItemDecoration(mDividerItemDecoration);
    }

    private void loadSongs(String term) {
        emptyView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);

        SongManager songManager = new SongManager(getActivity().getApplicationContext());

        // RXjava disposable call and add subscription on result from contact list api
        mCompositeDisposable.add(songManager.doGetSongs(term).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<SongsResponse>() {

                    // on next item emitted from observable handling
                    @Override
                    public void onNext(@NonNull SongsResponse contactsResponse) {
                        handleResponse(contactsResponse);
                    }

                    // on error handling of observable
                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    // on complete call of observable when it is done with its task fully
                    @Override
                    public void onComplete() {

                    }
                }));
    }

    //handler for onclick contact item in recycle view item,and open contact detail activity
    @Override
    public void itemClick(View v, int position) {
        Intent intent = new Intent(getActivity(), SongDetailActivity.class);
        intent.putExtra(AppConstants.SONG_DATA, (Parcelable) v.getTag());
        startActivity(intent);
    }

    // handler for contacts list returned from Rxjava disposable to handle data returned from server with contact list
    private void handleResponse(SongsResponse contactList) {
        retainData = (ArrayList<Song>) contactList.getResults();
        int resultCount = contactList.getResultCount();

        if (resultCount > 0) {
            adapter.setResults(retainData);
            mProgressView.setVisibility(View.GONE);
            emptyView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mProgressView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setText(R.string.no_result_found);
            emptyView.setVisibility(View.VISIBLE);

        }
    }

    // display error to user if there is some problem with server call for contactlist data retrieval
    private void handleError(Throwable error) {
        mProgressView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        errorSnackbar = Snackbar.make(mRecyclerView, R.string.no_internet_title, Snackbar.LENGTH_LONG);
        errorSnackbar.show();
    }

    @Override
    public void onResume() {
        if (retainData.size() > 0) {
            adapter.setResults(retainData);
            mProgressView.setVisibility(View.GONE);
            emptyView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        super.onResume();
    }
}
